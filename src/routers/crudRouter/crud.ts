
import fs from 'fs/promises';
import express, { RequestHandler } from 'express';
const router = express.Router();
import {checkIfExists, generateId, saveDb} from '../../etc/helpers.js';
import log from '@ajar/marker';
import { CustomRequest, User } from '../../etc/types.js';

const functionLogger : RequestHandler =  async (req :express.Request,res :express.Response,next) =>{
    log.cyan('Writing to the log...')
    const path = 'src/LOG/logfile.txt';
    const flag = await checkIfExists(path);
    if (!flag){
        console.log('Creating log file...')
        await fs.writeFile(path,JSON.stringify([]),'utf-8');
    }
    const content = req.method + ' ' + req.originalUrl + ' ' + Date.now() + '' + '\r\n';
    await fs.appendFile(path,content);
    log.green('Completed writing to log!');
    next();
}

const loader = async (req:any,res:express.Response,next : Function) =>{
    const path = 'src/DB/db.json';
    const flag = await checkIfExists(path);
    if (!flag){
        console.log('Creating db file...')
        await fs.writeFile(path,JSON.stringify([]),'utf-8');
    }
    const json = await fs.readFile(path,'utf-8');
    const result = await JSON.parse(json) as User[];
    (req as CustomRequest).db = result;
    (req as CustomRequest).givenPath = path;
    next();
}
router.use(loader);
router.use(functionLogger);

router.post('/adduser',async (req:any,res : express.Response)=>{
    const db = req.db;
    const {user_name,password,email} = req.body 
    const responseObj : User = {
        id : generateId(),
        user_name : user_name,
        password : password,
        email : email
    }
    db.push(responseObj);
    await saveDb(req.givenPath,db);
    res.status(200).send(responseObj);
})

router.get('/getusers',async(req:any,res:express.Response)=>{
    const db = req.db;
    res.status(200).send(db);
})

router.post('/updateuser/:id', async(req:any,res:express.Response)=>{
    let db = req.db;
    const id = req.params.id;
    const user = db.find((user : User)=>user.id === id);
    if (!user) {
        res.status(500).json(`Could not find user with id : ${id}`);
    } else {
        user.user_name = req.body.user_name;
        user.password = req.body.password;
        user.email = req.body.email;
        res.status(200).json(user);
    }
})

router.delete('/deleteuser/:id',async(req:any,res:express.Response)=>{
    let db = req.db;
    const id = req.params.id;
    const user = db.find((user : User)=>user.id === id);
    if (!user) {
        res.status(500).json(`Could not find user with id : ${id}`);
    } else {
        db = db.filter((user : User)=> user.id !== id);
        await saveDb(req.givenPath,db);
        res.status(200).json(`Deleted user with given id ${id}`);
    }
})


export default router;