import express, { RequestHandler } from 'express';
const router = express.Router();
import log from '@ajar/marker';

const functionLogger : RequestHandler = (req :express.Request,res :express.Response,next) =>{
    log.cyan('Only logs at adding new user..')
    next();
}
router.post('/newUser',functionLogger,(req :express.Request,res :express.Response)=>{
    const {user_id,password,email} = req.body 
    const responseObj = {
        user_id : user_id,
        password : password,
        email : email
    }
    res.status(200).send(responseObj)
})

router.get('/search',(req :express.Request,res :express.Response)=>{
    const food = req.query.food;
    const town = req.query.town;
    const responseObj = {
        food : food,
        town : town
    }
    res.status(200).send(responseObj);
})

router.get('/search/:id', (req :express.Request,res :express.Response)=>{
    const param = req.params.id;
    console.log(param)
    res.status(200).send(param);
})

export default router;