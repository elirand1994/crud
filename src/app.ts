import express from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';
import crudRouter from './routers/crudRouter/crud.js';

const appRouter = express.Router();

const {PORT, HOST } = process.env;
const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use('/crud',crudRouter);
app.use(appRouter);

appRouter.use('*', (req,res)=>{
    res.status(404).send("URL is broken!")
})

app.listen(Number(PORT), HOST as string,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});