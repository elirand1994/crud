export interface User {
    id : string,
    user_name : string,
    password : number,
    email : string
}

export interface CustomRequest extends Request {
    db : User[],
    givenPath: string
}